#
# spec file for package ULS Client for Oracle Databases
#
# Copyright (c) 2016 - 2024 Roveda roveda@universal-logging-system.org
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#
# Please submit bugfixes or comments to roveda@universal-logging-system.org

Summary:   ULS Client for Oracle Databases
Name:      uls-client-oracle
Version:   SETVERSION
Release:   SETRELEASE
License:   GPLv3
Group:     Productivity/Databases/Tools
Source:    SETSOURCE
BuildArch: noarch
BuildRoot: /dev/shm/uls-client-oracle-makerpm
Requires:  uls-client >= 3.15-9, perl >= 5, perl-Config-IniFiles
#DATAPORT#Packager: Rüdiger Mähl <ruediger.maehl@dataport.de>
#NODATAPORT#Packager: Rüdiger Mähl <roveda@universal-logging-system.org>
#DATAPORT#Vendor: Dataport AöR
#NODATAPORT#Vendor: Universal-Logging-System.org

# -------------------------------------------------------------------
%description
The ULS Client for Oracle databases is the GPLv3-based agent for gathering
runtime metrics and status information and logging configuration settings
on Oracle database instances running on Unix-like operating systems.
All gathered data is sent to an ULS server.
It works for the 12c and 19c versions of the Oracle RDBMS.
See www.universal-logging-system.org for more information

# -------------------------------------------------------------------
%prep
:

# -------------------------------------------------------------------
%setup
:

# -------------------------------------------------------------------
%build
echo "RPM_BUILD_ROOT: $RPM_BUILD_ROOT"

# -------------------------------------------------------------------
%install
if [[ -n "$RPM_BUILD_ROOT" ]]
 then
  mkdir -p $RPM_BUILD_ROOT
  cp -a `ls| grep -v 'SRC'` $RPM_BUILD_ROOT/
else
  rm -rf /dev/shm/uls-client-oracle-makerpm
  mkdir -p /dev/shm/uls-client-oracle-makerpm
  cp -a * /dev/shm/uls-client-oracle-makerpm
fi


# -------------------------------------------------------------------
%clean
rm -rf /dev/shm/uls-client-oracle-makerpm


# -------------------------------------------------------------------
%files

%attr(755,oracle,oinstall) %dir /etc/uls/oracle
%attr(755,oracle,oinstall) %dir /etc/uls/oracle/oracle_env.d
# %attr(-,oracle,oinstall) /etc/uls/oracle/standard.conf
%attr(444,oracle,oinstall) /etc/uls/oracle/standard.conf

%attr(-,oracle,oinstall) /usr/share/ulsclientoracle
%attr(-,oracle,oinstall) /var/tmp/ulsclientoracle

# -------------------------------------------------------------------
# %pre
#            install   upgrade   uninstall
# %pre       $1 == 1   $1 == 2       (N/A)

# if [[ "$1" = "1" ]] ; then
#   # new(!) installation
#
# fi
# :

# -------------------------------------------------------------------
%post
#
#            install   upgrade   uninstall
# %post      $1 == 1   $1 == 2       (N/A)

WORKDIR=/var/tmp/ulsclientoracle

# -----
# install

if [[ "$1" = "1" ]] ; then
  # new(!) installation

  # -----
  # Check for existing user 'oracle'

  u=oracle

  if id "$u" >/dev/null 2>&1; then
    echo "User '$u' exists"
  else
    echo "User '$u' does not exist => rpm cannot be installed"
    exit 1
  fi

  # -----
  # Comment all watch_oracle entries in existing cron files,
  # but not in those files named "uls*".
  # Shrink possibly existing numerous '#' at the beginning to one single
  sed -i '/watch_oracle/s/^#*/#/' $(find /etc/cron.d -maxdepth 1 -type f ! -name "uls*")


  # Set links to all existing oracle environments,
  # if a matching directory exists.
  #
  # Find the ORACLE_SIDs of all oracle_env_* files in oracle's home directory
  #
  # SIDS=$(awk -F = '/ORACLE_SID=/ {print $2}' ~oracle/oracle_env_* | sort -u)
  # (that does not work for an initial installation)

  SIDS=""
  for f in $(find ~oracle -maxdepth 1 -type f -name "oracle_env_*" ! -name "*.*") ; do
    SID=$(awk -F = '/ORACLE_SID=/ {print $2}' $f)
    SIDS="$SIDS $SID"
  done
  SIDS=$(echo $SIDS)

  if [ -z "$SIDS" ] ; then
    echo "No ORACLE_SIDs found."
  else
    echo "Found the ORACLE_SIDs: $SIDS"
  fi

  # For all ORACLE_SIDs
  for sid in $SIDS ; do

    # Create work directory, if it does not already exist
    [ ! -d $WORKDIR/${sid} ] && mkdir $WORKDIR/${sid}

    # Are the old oracle_tools installed?
    if [ -d /oradata/${sid}/oracle_tools ] ; then

      echo "ORACLE_SID ${sid} found, trying to re-use work files of oracle_tools."

      # Work files of previous oracle_tools
      if [ -d /oradata/${sid}/oracle_tools/var/ ] ; then

        # Copy the current work files to the new work directory

        # _watch_oracle_orcl.alert_log_position (sid is in file name)
        echo "Checking for work files containing an SID..."
        F=$(mktemp)
        find /oracle/admin/${sid}/oracle_tools/var/ -name "_watch_oracle_${sid}*" > $F
        while read f ; do
          cp -ua  "$f"  $WORKDIR/${sid}/
          # -u|--update: only if source is newer than destination or does not exist
        done < $F
        echo "Done."

        # _watch_oracle.alert_log_position (no sid in file name)
        echo "Checking for work files without SID..."
        find /oracle/admin/${sid}/oracle_tools/var/ -name "_watch_oracle.*" > $F
        while read f ; do
          fn="${f##*/}"
          ex="${fn#*.}"
          fn="${fn%%.*}"
          cp -ua "$f"  "$WORKDIR/${sid}/${fn}_${sid}.$ex"
          # -u|--update: only if source is newer than destination or does not exist
        done < $F
        echo "Done."

        rm $F

      fi
    fi

    # Create links to the environment scripts
    ln -sf ~oracle/oracle_env_${sid} /etc/uls/oracle/oracle_env.d/oracle_env_${sid}
    echo "Notice: link to ~oracle/oracle_env_${sid} created."
  done

  # Set oracle as owner of directory
  chown -R oracle:oinstall $WORKDIR

fi

# -----
# install or upgrade

if [[ "$1" = "1" || "$1" = "2" ]] ; then
  # Write an old timestamp into the ONCE_A_DAY files, that will lead to
  # the transmission of new script versions and some more values.
  F=$(mktemp)
  find $WORKDIR/ -name "*.nineoclock" > $F
  while read f ; do
    echo "2001-01-01 01:01:01" > $f
  done < $F
  rm $F
  #
  # on old installations modify lfa4uls_oracle_alert_logs.conf
  if [[ ! -d /db ]] && grep -q '^inputfiles = /db/' /etc/uls/lfa4uls_oracle_alert_logs.conf; then
    sed -i 's|^inputfiles = /db/\*|inputfiles = /oradata|' /etc/uls/lfa4uls_oracle_alert_logs.conf
  fi
  if [[ -f /etc/uls/lfa4uls_oracle_alert_logs.conf ]]; then
    systemctl enable lfa4uls@oracle_alert_logs
    systemctl restart lfa4uls@oracle_alert_logs
  fi
fi

# -----
# upgrade
if [[ "$1" = "2" ]] ; then
  # Be sure to change the ownership of these directories to oracle.
  # (They were left to root in older versions.)
  if [ -d /usr/share/ulsclientoracle ] ; then
    chown oracle:oinstall /usr/share/ulsclientoracle
  fi
  if [ -d /var/tmp/ulsclientoracle ] ; then
    chown oracle:oinstall /var/tmp/ulsclientoracle
  fi
fi

# -------------------------------------------------------------------
%preun
#
#            install   upgrade   uninstall
# %preun       (N/A)   $1 == 1     $1 == 0

if [[ "$1" = "0" ]] ; then

  # Delete all links to Oracle environment scripts
  [ -d /etc/uls/oracle/oracle_env.d ] && find /etc/uls/oracle/oracle_env.d/ -name "*" -delete

  # Delete all non standard conf files (if any exists)
  [ -d /etc/uls/oracle ] && find /etc/uls/oracle/ ! -name standard.conf -delete

  # Delete the directories containing the work files for every SID
  [ -d /var/tmp/ulsclientoracle ] && rm -r /var/tmp/ulsclientoracle/*

fi
:


# -------------------------------------------------------------------
%changelog

* Tue Oct 29 2024 Roveda <roveda@universal-logging-system.org> 1.0-47
2024-10-29: Removed several metrics, or changed to optional metrics in watch_oracle.pl

* Mon Sep 02 2024 Roveda <roveda@universal-logging-system.org> 1.0-46
2024-09-02: Removed syntax error in watch_oracle.pl
            lfa4uls_oracle_excludes.conf has also been changed.

* Tue Jul 30 2024 Roveda <roveda@universal-logging-system.org> 1.0-45
2024-07-30: Take notice of FAILED_LOGIN_REPORT in watch_oracle.pl

* Fri Feb  9 2024 Sandix <sandix@universal-logging-system.org> 1.0-43
2024-02-09: /etc/uls/lfa4uls_oracle_alert_logs.conf.template
            Send lines with "<PDBNAME>(n):Errors in file .*trc" to CDB-ULS-Section

* Thu Jan 30 2024 Sandix <sandix@universal-logging-system.org> 1.0-43
2024-01-30: /etc/uls/lfa4uls_oracle_alert_logs.conf.template
            Send pdb names in lowercase characters.

* Thu Dec 28 2023 Roveda <roveda@universal-logging-system.org> 1.0-43
2023-12-28: That has not worked, NO_ALERT_LOG was not set. Now corrected in /etc/uls/oracle/standard.conf.
            More excludes in /etc/uls/lfa4uls_oracle_excludes.conf added and changed for PDB errors.

* Wed Sep 20 2023 Roveda <roveda@universal-logging-system.org> 1.0-42
2023-09-20: NO_ALERT_LOG als Default in /etc/uls/oracle/standard.conf gesetzt.

* Fri Feb 24 2023 Roveda <roveda@universal-logging-system.org> 1.0-41
2023-02-22: Removed the debug setting in watch_oracle.pl

* Wed Feb 01 2023 Roveda <roveda@universal-logging-system.org> 1.0-40
2023-02-01: Changed 'data files' to cover multitenant scenarios in watch_oracle.pl (gitlab issue #13).
2022-12-14: Added 'RESTRICTED' mode in watch_oracle.pl (gitlab issue #18).

* Wed Sep 21 2022 Roveda <roveda@universal-logging-system.org> 1.0-39
2022-09-21: Changed watch_oracle.pl, renamed test step 'db_files' to 'data files' and more (gitlab issue #17).
2022-09-21: Added missing changelog info about db_files in this .spec file.

* Sun Sep 18 2022 Roveda <roveda@universal-logging-system.org> 1.0-38
2022-09-17: Workaround (hopefully) for wrong results in V$TEMP_SPACE_HEADER in watch_oracle.pl (gitlab issue #12).
2022-09-15: Added max usable sga size for pdbs in watch_oracle.pl
2022-09-11: Added current number of data files and parameter db_files to watch_oracle.pl.
2022-08-15: Added check for existing oracle user in .spec file.
2022-07-13: Info: project moved to gitlab.

* Tue Jun 21 2022 Roveda <roveda@universal-logging-system.org> 1.0-37
2022-06-21: Added BLOCKING_SESSIONS_ALWAYS to configuration file and incorporation in
watch_oracle.pl, blocking_sessions() (github issue #11).

* Tue Apr 12 2022 Roveda <roveda@universal-logging-system.org> 1.0-36
2022-04-12: Changed 'Info PDB - open mode' to 'Info PDB - cdb role - pdb open mode' in watch_oracle.pl.

* Fri Feb 25 2022 Roveda <roveda@universal-logging-system.org> 1.0-35
2022-02-25: Added encoding guessing in Uls2.pm.
2022-02-01: Added lfa4uls functionality to process the alert.log.

* Sun Jan 16 2022 Roveda <roveda@universal-logging-system.org> 1.0-34
2022-01-16: watch_oracle.pl: added documentation, optimized access to
audit entries by using EVENT_TIMESTAMP_UTC.

* Thu Dec 09 2021 Roveda <roveda@universal-logging-system.org> 1.0-33
2021-12-09: Moved 'set feedback off' to beginnning of sql commands and
changed print output in sub exec_sql() in all perl scripts.
Unsetting SQLPATH and ORACLE_PATH in all bash scripts after having sourced the environment file
to avoid executing the login.sql which may include unwanted settings.
2021-12-06: Implemented the changes from HJF into Misc.pm

* Sat Dec 04 2021 Roveda <roveda@universal-logging-system.org> 1.0-32
2021-12-04: New parameter ADDITIONAL_ENCODINGS in etc/uls/oracle/standard.conf.
Implemented this parameter in watch_oracle.pl to allow customizing of additional
encoding checks of alert.log lines (which depends on the database character set).

* Fri Dec 03 2021 Roveda <roveda@universal-logging-system.org> 1.0-31
2021-12-03: Trying to convert strangely encoded lines of the alert.log to
Perl's internal encoding to preserve umlaute in watch_oracle.pl

* Thu Dec 02 2021 Roveda <roveda@universal-logging-system.org> 1.0-30
2021-12-02: Changed to LANG=en_US.UTF-8 in all bash scripts, determine the current
directory by the use of 'readlink', added echoerr() as local function.

* Sat Nov 27 2021 Roveda <roveda@universal-logging-system.org> 1.0-29
2021-11-27: Changed default LANG setting from C to en_US.UTF-8 in all .sh scripts.
Added full UTF-8 support to all perl scripts Umlaute should now be processed correctly
in output to files sent to ULS.
2021-11-16: Added detail 'database role - status' in watch_oracle.pl. The minus sign
is used as separator instead of the comma, which always led to misinterpretations in
limit and report definitions ('database role, status' will be removed in a later version).

* Wed Oct 20 2021 Roveda <roveda@universal-logging-system.org> 1.0-28
2021-10-20: Number of max processes will be re-sent to ULS, if the value has changed.

* Sun Oct 17 2021 Roveda <roveda@universal-logging-system.org> 1.0-27
2021-10-17: Corrected the retrieval of usage in tablespace_usage().

* Sun May 30 2021 Roveda <roveda@universal-logging-system.org> 1.0-26
2021-05-29: Added NO_ALERT_LOG as option in configuration file.

* Wed Mar 17 2021 Roveda <roveda@universal-logging-system.org> 1.0-25
2021-03-17: Changed 'blocking sessions' in watch_oracle.pl (now version 1.00).
2021-02-05: Added the number of inactive online redo logs to watch_oracle.pl.

* Wed Jan 20 2021 Roveda <roveda@universal-logging-system.org> 1.0-24
2021-01-18: The sga size is only sent once a day in watch_oracle.pl if it does not change.

* Thu Nov 26 2020 Roveda <roveda@universal-logging-system.org> 1.0-23
2020-11-26: implemented a smooth change to new workfile in watch_oracle.pl,
no reset to beginning of alert.log.

* Thu Oct 01 2020 Roveda <roveda@universal-logging-system.org> 1.0-22
2020-10-01: merged the preliminary watch_oracle_pdbs.pl script into watch_oracle.pl.
Changed crontab file back to one entry.

* Tue Sep 01 2020 Roveda <roveda@universal-logging-system.org> 1.0-21
2020-09-01: Wrong timestamp for 'maintenance window start-stop' corrected.
2020-08-30: Enhanced version of watch_oracle_pdbs.pl.
2020-08-16: Added '--force' to compress commands in Misc.pm
2020-08-01: Added watch_oracle_pdbs.pl and .sh as a beta version.
watch_oracle.pl: For Oracle version 18+ the full version is retrieved.

* Thu Mar 05 2020 Roveda <roveda@universal-logging-system.org> 1.0-20
2020-03-05: watch_oracle.pl: implemented a workaround for 'ORA-' expressions in
dba_autotask_job_history.job_info in sub auto_optimizer_stats_collection().
Added special filter for alert.log entries witch contain ORA- expressions,
but as patch description and not as real error entry.
2019-12-27: watch_oracle.pl: implemented a workaround for 'ORA-' expressions in
dba_autotask_job_history.job_info in sub auto_optimizer_stats_collection().
2019-12-04: Changed permissions on /etc/uls/oracle/standard.conf to 444.

* Wed Nov 27 2019 Roveda <roveda@universal-logging-system.org> 1.0-19
2019-11-27: watch_oracle.pl: debugged data guard information.
2019-10-22: watch_oracle.pl: Added extended information to auto optimizer stats collection.

* Tue Oct 15 2019 Roveda <roveda@universal-logging-system.org> 1.0-18
2019-10-15: Extended watch_oracle.pl by DataGuard and auto optimizer stats collection information.

* Mon Sep 23 2019 Roveda <roveda@universal-logging-system.org> 1.0-17
Debugged watch_oracle.pl: added a missing semi-colon.

* Thu Sep 19 2019 Roveda <roveda@universal-logging-system.org> 1.0-16
2018-10-03: Removed obsolete calculation from sga() in watch_oracle.pl
2019-09-19: Added the 'max processes since startup' in watch_oracle.pl

* Sun Mar 11 2018 Roveda <roveda@universal-logging-system.org> 1.0-15
Added monitoring of failed logins for unified auditing.

* Wed Feb 14 2018 Roveda <roveda@universal-logging-system.org> 1.0-14
Changed checks for successful sourcing the environment in all shell scripts.

* Fri Jan 26 2018 Roveda <roveda@universal-logging-system.org> 1.0-13
Changed watch_oracle.pl, added 'parse count (hard)', 'transaction rollbacks' and 'user rollbacks' to system statistics.
Updated the versions of the used perl modules.
%used for tablespaces is renamed to %used_pms (of potential max size).
A correct error message is given if the alert.log could not be found (and not '-not found-').

* Thu Oct 19 2017 Roveda <roveda@universal-logging-system.org> 1.0-12
Changed the find to omit environment scripts that have an extension (like a datetimestamp or .old).

* Fri Sep 29 2017 Roveda <roveda@universal-logging-system.org> 1.0-11
The package threw an error when no environment scripts already existed.
Finding of environment scripts changed to 'find' instead of direct globbing.

* Mon Sep 25 2017 Roveda <roveda@universal-logging-system.org> 1.0-10
Changed watch_oracle.pl: added $ORACLE_VERSION_3D for exact version comparisons.
Column DELEGATE_OPTION does not exist in Oracle versions lower than 12.1.0.2,
COMMON not in versions lower than 12.1.0.1. So i just skip admin_db_user() for all
Oracle versions lower than 12.1.0.2.

* Tue Aug 29 2017 Roveda <roveda@universal-logging-system.org> 1.0-9
Added uls_settings.pl and a matching bash script.
Debugged the list of non-oracle database user with administrative privileges.
Added detail 'potential max size' to tablespace values.

* Tue May 30 2017 Roveda <roveda@universal-logging-system.org> 1.0-8
Changed the ownership of directory /var/tmp/ulsclientoracle to oracle:oinstall,
the scripts are not able to create a working directory if it is owned by root.
Only relevant for (later) added instances, not for initial installation.

* Fri May 26 2017 Roveda <roveda@universal-logging-system.org> 1.0-7
Changed watch_oracle.pl, added a list of non-oracle user who possess
administrative privileges.

* Thu Mar 30 2017 Roveda <roveda@universal-logging-system.org> 1.0-6
Reset the time stamp within the ONCE_A_DAY file for an upgrade and
also on initial install.

* Thu Feb 09 2017 Roveda <roveda@universal-logging-system.org> 1.0-5
Changed the script oracle_instance.sh to watch_oracle.sh and conformed
all other scripts and crontab to that new script name.
Added 'schema information' with 'allocated space' as new teststep to watch_oracle.pl.
Debugged that after first test.
Updated watch_oracle.pl to correctly save unknown values if the database is not
available or the sql commands cannot be executed.
Extended Uls2.pm for more verbose output in copy_files().

* Tue Jan 31 2017 Roveda <roveda@universal-logging-system.org> 1.0-4
Changed file permissions, changed "Requires:"

* Mon Jul 11 2016 Roveda <roveda@universal-logging-system.org> 1.0-3
Added copyright text and description to this spec file.
Added correct required packages.

* Wed Jul 06 2016 Roveda <roveda@universal-logging-system.org> 1.0-2
Debugged bytes2gb() in Misc.pm

* Tue Jul 05 2016 Roveda <roveda@universal-logging-system.org> 1.0-1
Creation of 1.0-1 as rpm
Extracted the watch_oracle relevant entries from original CHANGES file.
Splitted up the oracle_tools to 'ULS Client for Oracle Databases' (uls-client-oracle)
and 'Operational Tools for Oracle Databases' (optools-oracle).
The SID-specific .conf file must be sid.conf in /etc/uls/oracle

* Tue Mar 22 2016 Roveda <roveda@universal-logging-system.org>
Made the get_config2() in Misc.pm safe to read files without any sections.
The perl scripts exited when trying to read the initially empty oracle_tools_SID.conf file.

* Mon Mar 21 2016 Roveda <roveda@universal-logging-system.org>
Made the oracle_tools.conf read-only, because non-standard parameters should be
defined in an oracle_tools_SID.conf file. That allows better and more reliable
automatic updates of the oracle_tools.

* Fri Mar 18 2016 Roveda <roveda@universal-logging-system.org>
Changed all .pl to support a second, ORACLE_SID-specific configuration file.
That is the preparation for fully automatic updates of the oracle_tools:
All defaults are defined in oracle_tools.conf, if you need any other settings,
you create an oracle_tools_$ORACLE_SID.conf file which ONLY contains the
deviant sections and parameters (do not forget the section!).

* Thu Sep 24 2015 Roveda <roveda@universal-logging-system.org>
Re-worked the sql command for successful and failed logins in watch_oracle.pl.

* Sun Sep 20 2015 Roveda <roveda@universal-logging-system.org>
Added information from registry$history to ora_dbinfo.
Added successful logins to watch_oracle.

* Thu Feb 12 2015 Roveda <roveda@universal-logging-system.org>
Debugged scheduler_details() in watch_oracle.pl
